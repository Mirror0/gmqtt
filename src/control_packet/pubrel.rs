use core::convert::TryFrom;

use heapless::Vec;

use crate::{utils::*, MqttError, Pid};

use super::{property, PropertyType};

/// A PUBREL packet is the response to a PUBREC packet. It is the third packet of the QoS 2 protocol exchange.
///
/// [MQTT5 SPECIFICATION](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901141)
#[derive(Debug, Clone, PartialEq)]
pub struct PubRel<'a> {
    /// Packet Identifier from the PUBLISH packet that is being acknowledged.
    pub pid: Pid,

    /// Byte 3 in the Variable Header is the PUBREL Reason Code.
    pub reason: PubRelReason,
    pub properties: Option<PubRelProperties<'a>>,
}

impl<'a> PubRel<'a> {
    pub fn new(pid: Pid) -> PubRel<'a> {
        PubRel {
            pid,
            reason: PubRelReason::Success,
            properties: None,
        }
    }

    pub fn read(buf: &'a [u8], offset: &mut usize) -> Result<Self, MqttError> {
        let pid = Pid::read(buf, offset)?;
        let reason = PubRelReason::try_from(buf[*offset])?;

        // move offset to properties_len field
        *offset += 1;

        Ok(PubRel {
            pid,
            reason,
            properties: PubRelProperties::read(buf, offset)?,
        })
    }

    pub fn write(&self, buf: &mut [u8], offset: &mut usize) -> Result<usize, MqttError> {
        check_remaining(buf, offset, 4)?;

        // PubRel (0x62);
        let header: u8 = 0x62;

        write_u8(buf, offset, header);
        let write_len = write_length(buf, offset, self.len())?;
        self.pid.write(buf, offset);
        write_u8(buf, offset, self.reason as u8);

        match &self.properties {
            Some(properties) => properties.write(buf, offset)?,
            None => write_u8(buf, offset, 0),
        }

        Ok(write_len)
    }

    fn len(&self) -> usize {
        let mut len = 2 + 1; // pkid + reason

        // If there are no properties, sending reason code is optional
        if self.reason == PubRelReason::Success && self.properties.is_none() {
            return 2;
        }

        match &self.properties {
            Some(properties) => {
                let properties_len = properties.len();
                let properties_len_len = len_len(properties_len);
                len += properties_len_len + properties_len;
            }
            None => {
                // just 1 byte representing 0 len
                len += 1;
            }
        }

        len
    }
}

/// [MQTT5 SPECIFICATION](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901145)
#[derive(Debug, Clone, PartialEq, Default)]
pub struct PubRelProperties<'a> {
    /// `31 (0x1F)` Byte, Identifier of the Reason String.
    ///
    /// Followed by the UTF-8 Encoded String representing the reason associated with this response.
    pub reason_string: Option<&'a str>,

    /// `38 (0x26)` Byte, Identifier of the User Property.
    ///
    /// Followed by UTF-8 String Pair.
    pub user_properties: Vec<(&'a str, &'a str), 32>,
}
impl<'a> PubRelProperties<'a> {
    pub fn read(buf: &'a [u8], offset: &mut usize) -> Result<Option<Self>, MqttError> {
        let mut properties = Self::default();

        let properties_len = buf[*offset];

        if properties_len == 0 {
            return Ok(None);
        }

        let mut cursor: usize = 1;

        while cursor < properties_len.into() {
            let mut prop_offset = *offset + cursor;
            let prop = buf[prop_offset];

            prop_offset += 1;
            cursor += 1;
            match property(prop)? {
                PropertyType::ReasonString => {
                    let reason = read_str(buf, &mut prop_offset)?;
                    cursor += 2 + reason.len();
                    properties.reason_string = Some(reason);
                }

                PropertyType::UserProperty => {
                    let key = read_str(buf, &mut prop_offset)?;
                    let value = read_str(buf, &mut prop_offset)?;
                    cursor += 2 + key.len() + 2 + value.len();
                    properties
                        .user_properties
                        .push((key, value))
                        .map_err(|_| MqttError::TooManyUserProperties)?;
                }

                prop => return Err(MqttError::InvalidPropertyType(prop)),
            }
        }

        *offset += properties.len();
        Ok(Some(properties))
    }
    pub fn write(&self, buf: &mut [u8], offset: &mut usize) -> Result<(), MqttError> {
        write_length(buf, offset, self.len())?;
        if let Some(reason_string) = self.reason_string {
            write_u8(buf, offset, PropertyType::ReasonString as u8);
            write_bytes_with_len(buf, offset, reason_string.as_bytes());
        }

        for (key, value) in self.user_properties.iter() {
            write_u8(buf, offset, PropertyType::UserProperty as u8);
            write_bytes_with_len(buf, offset, key.as_bytes());
            write_bytes_with_len(buf, offset, value.as_bytes());
        }

        Ok(())
    }

    fn len(&self) -> usize {
        let mut len = 0;

        if let Some(reason) = &self.reason_string {
            len += 1 + 2 + reason.len();
        }

        for (key, value) in self.user_properties.iter() {
            len += 1 + 2 + key.len() + 2 + value.len();
        }

        len
    }
}

/// [MQTT5 SPECIFICATION](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901144)
#[derive(Debug, Clone, Copy, PartialEq)]
#[repr(u8)]
pub enum PubRelReason {
    /// Message released.
    Success = 0,

    /// The Packet Identifier is not known.
    /// This is not an error during recovery, but at other times indicates a mismatch between the Session State on the Client and Server.
    PacketIdentifierNotFound = 146,
}
impl TryFrom<u8> for PubRelReason {
    type Error = MqttError;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        let code = match value {
            0 => PubRelReason::Success,
            146 => PubRelReason::PacketIdentifierNotFound,
            code => return Err(MqttError::InvalidReasonCode(code)),
        };

        Ok(code)
    }
}

#[cfg(test)]
mod tests {
    use super::{PubRel, PubRelProperties, PubRelReason};
    use crate::Pid;

    fn sample<'a>() -> PubRel<'a> {
        let properties = PubRelProperties {
            reason_string: Some("test"),
            user_properties: heapless::Vec::<_, 32>::from_slice(&[("test", "test")]).unwrap(),
        };

        PubRel {
            pid: Pid::new(42),
            reason: PubRelReason::PacketIdentifierNotFound,
            properties: Some(properties),
        }
    }

    fn sample_bytes() -> Vec<u8> {
        vec![
            0x62, // payload type
            0x18, // remaining length
            0x00, 0x2a, // packet id
            0x92, // reason
            0x14, // properties len
            0x1f, 0x00, 0x04, 0x74, 0x65, 0x73, 0x74, // reason_string
            0x26, 0x00, 0x04, 0x74, 0x65, 0x73, 0x74, 0x00, 0x04, 0x74, 0x65, 0x73,
            0x74, // user properties
        ]
    }
    #[test]
    fn pubrel_len() {
        let expected = 24;
        let actual = sample().len();
        assert_eq!(expected, actual);
    }
    #[test]
    fn pubrel_encode() {
        let expected = sample_bytes();
        let mut buf = [0u8; 26];
        sample().write(&mut buf, &mut 0).unwrap();

        assert_eq!(expected.as_slice(), buf);
    }

    #[test]
    fn pubrel_decode() {
        let bytes = &sample_bytes()[2..];
        let expected = sample();
        let actual = PubRel::read(bytes, &mut 0).unwrap();
        assert_eq!(expected, actual);
    }

    #[test]
    fn pubrel_props_encode() {
        let expected = &sample_bytes()[5..];
        let mut buf = [0u8; 21];
        sample()
            .properties
            .unwrap()
            .write(&mut buf, &mut 0)
            .unwrap();
        assert_eq!(expected.len(), buf.len());

        assert_eq!(expected, buf);
    }

    #[test]
    fn pubrel_props_decode() {
        let bytes = &sample_bytes()[5..];
        let expected = sample().properties.unwrap();
        let actual = PubRelProperties::read(bytes, &mut 0).unwrap().unwrap();
        assert_eq!(expected, actual);
    }

    fn sample2<'a>() -> PubRel<'a> {
        PubRel {
            pid: Pid::new(42),
            reason: PubRelReason::PacketIdentifierNotFound,
            properties: None,
        }
    }

    fn sample_bytes2() -> Vec<u8> {
        vec![
            0x62, // payload type
            0x04, // remaining length
            0x00, 0x2a, // packet id
            0x92, // reason
            0x00, // properties len
        ]
    }

    #[test]
    fn pubrel_encode_2() {
        let expected = sample_bytes2();
        let mut buf = [0u8; 6];
        sample2().write(&mut buf, &mut 0).unwrap();

        assert_eq!(expected.as_slice(), buf);
    }

    #[test]
    fn pubrel_decode_2() {
        let bytes = &sample_bytes2()[2..];
        let expected = sample2();
        let actual = PubRel::read(bytes, &mut 0).unwrap();
        assert_eq!(expected, actual);
    }
}
