use crate::{utils::*, MqttError};

/// [MQTT5 SPECIFICATION](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901195)
pub struct PingReq;
impl PingReq {
    pub fn write(buf: &mut [u8], offset: &mut usize) -> Result<usize, MqttError> {
        check_remaining(buf, offset, 2)?;

        let header: u8 = 0xC0;
        let length: u8 = 0x00;

        write_u8(buf, offset, header);
        write_u8(buf, offset, length);

        Ok(2)
    }
}

/// [MQTT5 SPECIFICATION](https://docs.oasis-open.org/mqtt/mqtt/v5.0/os/mqtt-v5.0-os.html#_Toc3901200)
pub struct PingResp;
impl PingResp {
    pub fn write(buf: &mut [u8], offset: &mut usize) -> Result<usize, MqttError> {
        check_remaining(buf, offset, 2)?;

        let header: u8 = 0xD0;
        let length: u8 = 0x00;

        write_u8(buf, offset, header);
        write_u8(buf, offset, length);

        Ok(2)
    }
}

#[cfg(test)]
mod tests {
    use super::{PingReq, PingResp};

    #[test]
    fn encode_req() {
        let mut buf = [0u8; 2];
        let expected = [0xC0, 0x00];
        PingReq::write(&mut buf, &mut 0).unwrap();
        assert_eq!(expected, buf);
    }

    #[test]
    fn encode_resp() {
        let mut buf = [0u8; 2];
        let expected = [0xD0, 0x00];
        PingResp::write(&mut buf, &mut 0).unwrap();
        assert_eq!(expected, buf);
    }
}
